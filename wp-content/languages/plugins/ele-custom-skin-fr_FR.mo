��    0      �  C         (     )     +     /  ~   L     �     �     �     �          (     /     7     D     I  :   \     �     �     �     �     �  
   �     �     �     �     �     �  
   �  "   �                    5  +   O     {     �     �     �     �     �     �     �     �                         "  )  *     T     V     Z  �   v      	     /	     L	     U	  $   g	     �	     �	     �	     �	     �	  L   �	  
   
     *
     2
     ;
     B
  
   J
     U
     Y
     _
     g
     m
  
   r
  1   }
     �
     �
  $   �
  $   �
  A        a     j     �     �     �     �     �  %   �  $        9     I     N     R     [     "             0   '   /                    
                         +                 	      $           *                                         -                       %          )          !   #   .   ,   (          &               1 404 <b>Alternating templates</b> <b>Ele Custom Skin</b> needs <b>Elementor</b> and <b>Elementor Pro</b> to work. Make sure you have them <b>both</b> installed. <i><b>n</b></i> th post Add Loop Template Arrows Arrows and Dots Create/edit a Loop Template Custom Default Display Mode Dots Dynamic Everywhere Get full features with <b><i>Ele Custom Skin PRO</i></b>,  Go Pro Hide Length Loop Masonry Navigation No None Off On Page Pagination Please select a default template!  Post Summary Same Height See <b>Pro</b> features Select a default template Set how many slides are scrolled per swipe. Show Show in Slider Single Slider Options Slides to Scroll Slides to Show Template for every  Unlock PRO features.  Using Dynamic Keywords Video Tutorial View Yes nth th post PO-Revision-Date: 2020-03-17 20:21:33+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: fr
Project-Id-Version: Plugins - Elementor Custom Skin - Development (trunk)
 1 404 <b>Modèles alternatifs</b> <b>Ele Custom Skin</b> nécessite <b>Elementor</b> et <b>Elementor Pro</b> pour fonctionner. Assurez-vous de les avoir installés <b>tous les deux</b>. <i><b>n</b></i>ième publication Ajouter un modèle de boucle Flèches Flèches & points Créer/modifier un modèle de boucle Personnalisé Par défaut Mode d’affichage Points Dynamique partout Obtenez toutes les fonctionnalités avec <b><i>Ele Custom Skin PRO</i></b>,  Passer Pro Masquer Longueur Boucle Masonry Navigation Non Aucun Inactif Actif Page Pagination Veuillez sélectionner un modèle par défaut !  Résumé de publication Même hauteur Voir les fonctionnalités <b>Pro</b> Sélectionnez un modèle par défaut Définissez le nombre de diapositives qui défilent par balayage. Afficher Afficher dans un diaporama Unique Options du diaporama Diapositives à faire défiler Diapositives à afficher Modèle pour chaque  Débloquer les fonctionnalités Pro.  Utilisation de mots-clés dynamiques Tutoriel vidéo Voir Oui énième nième publication 